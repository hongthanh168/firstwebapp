﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System;
using System.Collections.Generic;

namespace FirstWebApp.Models
{
    public class NhanVienMetadata
    {
        public int Id { get; set; }

        [Display(Name ="Họ và tên")]
        public string hoten { get; set; }

        [DataType(DataType.Date),Display(Name ="Ngày sinh"), DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime ngaysinh { get; set; }

        [Display(Name ="Địa chỉ thường trú")]
        public string thuongtru { get; set; }

        [DataType(DataType.Date), Display(Name = "Ngày vào làm"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> ngayvaolam { get; set; }

        [Display(Name ="Số CMND")]
        public string cmnd { get; set; }
    }
    public class sp_T_GetNhanVienInPhongBanMetadata
    {
        [Display(Name = "Họ tên")]
        public string hoten { get; set; }

        [Display(Name = "Ngày sinh"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime ngaysinh { get; set; }

        [Display(Name = "Địa chỉ")]
        public string thuongtru { get; set; }

        [Display(Name = "Ngày vào phòng"), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public System.DateTime ngayVao { get; set; }

        [Display(Name = "Tên phòng")]
        public string tenphong { get; set; }
    }
    public class DonViMetadata
    {
        public int id { get; set; }
        public string ten { get; set; }
        public Nullable<int> parentID { get; set; }                
    }
}