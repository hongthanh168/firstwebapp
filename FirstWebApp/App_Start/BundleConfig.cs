﻿using System.Web;
using System.Web.Optimization;

namespace FirstWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui")
                 .Include("~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/jqueryui")
               .Include("~/Content/themes/base/all.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //theme Sb-admin-2
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/sb-admin-2.css"));

            //Theme gentelella-master
            //bundles.Add(new StyleBundle("~/Content/css").Include(
            //          "~/Content/bootstrap.css",
            //          "~/Content/dai_duong_custom.css"));
            
            
            //thêm jquery plugin datatables
            bundles.Add(new ScriptBundle("~/vendors/DataTables/js").Include("~/vendors/DataTables/datatables.js"));
            bundles.Add(new StyleBundle("~/vendors/DataTables/css").Include("~/vendors/DataTables/datatables.css"));

            bundles.Add(new ScriptBundle("~/vendors/DataTables/Buttons/js").Include(
                "~/vendors/DataTables/Buttons/buttons.flash.min.js",
                "~/vendors/DataTables/Buttons/buttons.html5.min.js",
                "~/vendors/DataTables/Buttons/buttons.print.min.js",
                "~/vendors/DataTables/Buttons/dataTables.buttons.min.js",
                "~/vendors/DataTables/Buttons/jszip.min.js",
                "~/vendors/DataTables/Buttons/pdfmake.min.js",
                "~/vendors/DataTables/Buttons/vfs_fonts.js"
                ));
            bundles.Add(new StyleBundle("~/vendors/DataTables/Buttons/css").Include("~/vendors/DataTables/Buttons/buttons.dataTables.min.css"));

            //thêm font-awesome
            bundles.Add(new StyleBundle("~/vendors/fontawesome/css").Include("~/vendors/fontawesome/css/font-awesome.min.css"));

            //thêm jsTree
            bundles.Add(new ScriptBundle("~/vendors/jsTree/js").Include(
                        "~/vendors/jsTree/jstree.min.js"));

            bundles.Add(new StyleBundle("~/vendors/jsTree/css").Include(
                               "~/vendors/jsTree/themes/default/style.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/modalform").Include(
                       "~/Scripts/modalform.js"));

            //thêm metisMenu
            bundles.Add(new StyleBundle("~/vendors/metisMenu/dist").Include("~/vendors/metisMenu/dist/metisMenu.css"));
            bundles.Add(new ScriptBundle("~/bundles/metisMenu/dist").Include("~/vendors/metisMenu/dist/metisMenu.js"));
        }
    }
}
