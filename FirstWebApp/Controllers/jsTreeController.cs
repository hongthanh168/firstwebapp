﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FirstWebApp.Models;
using System.Data.Entity;
using System.Net;

namespace FirstWebApp.Controllers
{
    public class jsTreeController : Controller
    {
        private NhanSuEntities db = new NhanSuEntities();
        // GET: jsTree
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetTreeData()
        {
            
                List<JsTreeModel> nodes = new List<JsTreeModel>();
                //add những node có parentID null
                List<DonVi> dvs = db.DonVis.Where(x => x.parentID == null).ToList();
                foreach (DonVi dv in dvs)
                {
                    JsTreeModel node = new JsTreeModel();
                    node.id = dv.Id;
                    node.text = dv.ten;
                    SetChildren(node);
                    nodes.Add(node);
                }
                //AlreadyPopulated = true;
                return Json(nodes);
            
        }
        public void SetChildren(JsTreeModel node)
        {
            List<DonVi> dvs = db.DonVis.Where(x => x.parentID == node.id).ToList();
            foreach (DonVi dv in dvs)
            {
                JsTreeModel childnode = new JsTreeModel();
                childnode.id = dv.Id;
                childnode.text = dv.ten;
                SetChildren(childnode);
                node.children.Add(childnode);
            }
        }
        [HttpPost]
        public ActionResult DoJsTreeOperation(JsTreeOperationData data)
        {
            switch (data.Operation)
            {
                case JsTreeOperation.CopyNode:
                case JsTreeOperation.CreateNode:
                    //todo: save data
                    DonVi dv = new DonVi();
                    dv.parentID = int.Parse(data.ParentId);
                    dv.ten = data.Text;
                    db.DonVis.Add(dv);
                    db.SaveChanges();
                    return Json(new { id = dv.Id }, JsonRequestBehavior.AllowGet);

                case JsTreeOperation.DeleteNode:
                    //todo: save data
                    int id = int.Parse(data.Id);
                    DonVi donVi = db.DonVis.Find(id);
                    db.DonVis.Remove(donVi);
                    db.SaveChanges();
                    return Json(new { result = "ok" }, JsonRequestBehavior.AllowGet);

                case JsTreeOperation.MoveNode:
                    //todo: save data
                    int id3 = int.Parse(data.Id);
                    DonVi donVi3 = db.DonVis.Find(id3);
                    donVi3.parentID = int.Parse(data.ParentId);
                    db.Entry(donVi3).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { result = "ok" }, JsonRequestBehavior.AllowGet);                   

                case JsTreeOperation.RenameNode:
                    //kiểm tra có tên nào trùng không
                    if(db.DonVis.Any(x => x.ten == data.Text))
                    {
                        return Json(new { KetQua = false, ThongBao = "Bị trùng tên đơn vị" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //todo: save data
                        int id2 = int.Parse(data.Id);
                        DonVi donVi2 = db.DonVis.Find(id2);
                        donVi2.ten = data.Text;
                        db.Entry(donVi2).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json(new { KetQua = true, ThongBao = "Đã lưu vào cơ sở dữ liệu" }, JsonRequestBehavior.AllowGet);
                    }
                    

                default:
                    throw new InvalidOperationException(string.Format("{0} is not supported.", data.Operation));
            }
        }

    }
}